import api from './resources';

export default {
  /**
   * 搜索建议
   */
  searchSuggest() {
    return api.get('search/hot').then(res => res.data.result.hots)
  },
  /**
   * 发现页banner
   */
  getBannerData() {
    return api.get('banner').then(res => res.data.banners)
  },
  /**
   * 推荐歌单
   */
  getPresonalizedData() {
    return api.get('personalized').then(res => res.data.result)
  }
}
