import React, { Component } from 'react';
import { observer } from 'mobx-react';
import './../../scss/Discover/banner.scss';
import Swiper from 'swiper';
import 'swiper/dist/css/swiper.min.css';

@observer
class Banner extends Component {
  constructor (props,context) {
    super(props, context)
    
    this.state = {
      
    }
  }

  componentDidMount () {
    
  }
  render() {
    var { bannerList } = this.props;
    var slideStr='';
    if(bannerList.length>0){
      slideStr =  bannerList.map((item,index)=>{
        return (<a href={item.url} className="swiper-slide" key={'banner-' + index}><div className="slideBanner" style={{background: `url(${item.picUrl}) center/cover`}}></div></a>)
      })
      setTimeout(() => {
        new Swiper('.swiper-discover-banner', {
          loop: true,
          pagination: {
            el: '.swiper-pagination',
            clickable: true,
          },
          autoplay:{
            delay: 2500,
            disableOnInteraction: false,
          },
        })
      },100)
      
    } 
    return (
      <div className="Banner">
        <div className="swiper-container swiper-discover-banner">
          <div className="swiper-wrapper">
          {slideStr}
          </div>
          <div className="swiper-pagination"></div>
        </div>
        
      </div>
    );
  }
}


export default Banner;
