import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';
import './../../scss/Discover/recommend.scss';
import Banner from './banner';
import FourBtn from './fourBtn';
import RecommendMusicList from './recommendMusicList';

@inject('discoverStore')
@observer
class Recommend extends Component {

  constructor(props) {
    super(props);

    this.state = {
      recommendFourBtn: [
        {
          fontIcon: 'iconfont icon-shouyinji',
          name: '私人FM',
          path: ''
        },{
          fontIcon: 'iconfont icon-rili',
          name: '每日推荐',
          path: ''
        },{
          fontIcon: 'iconfont icon-gedanshouluon',
          name: '歌单',
          path: ''
        },{
          fontIcon: 'iconfont icon-paihang',
          name: '排行榜',
          path: ''
        },
      ]
    }
  }

  componentWillMount () {

  }
  render() {
    const {bannerList, presonalizedData} = this.props.discoverStore
    return (
      <div className='Recommend'>
        <div className='rebackground'></div>
        <Banner bannerList={bannerList}></Banner>
        <FourBtn recommendFourBtn={this.state.recommendFourBtn}></FourBtn>
        <RecommendMusicList presonalizedData={presonalizedData}></RecommendMusicList>
      </div>
    )
  }
}

export default Recommend;