import React, {Component} from 'react';
import { observer } from 'mobx-react';
import './../../scss/Discover/recommendMusicList.scss';
import CellTitle from './cellTitle';

@observer 
class RecommendMusicList extends Component {
  render() {
    return (
      <div className="RecommendMusicList">
        <CellTitle title={'推荐歌单'}></CellTitle>
        <div className="musicList">
        {
          this.props.presonalizedData.map((v, k) => {
            return (
              <div key={'cell-' + k} className="musicListCell">
                <img src={v.picUrl} alt={v.name}></img>
                <p>{v.name}</p>
              </div>
            )
          })
        }
        </div>
        
      </div>
    )
  }
}

export default RecommendMusicList;