
import React, { Component } from 'react';
import './../../scss/Discover/discover.scss';
import { inject, observer } from 'mobx-react';
import Search from './search';
import Tabbar from './tabbar';
import Recommend from './recommend';
import Swiper from 'swiper';
import 'swiper/dist/css/swiper.min.css';

var disSwiper

@inject('discoverStore')
@observer
class Discover extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentWillMount () {
    const { getSearchSuggestData, searchHot, getbannerListData, getPresonalizedData} = this.props.discoverStore;
    if (!searchHot) {
      getSearchSuggestData();
      getbannerListData();
      getPresonalizedData();
    }
    
  }
  
  componentDidMount() {
    const { changeTabbar } = this.props.discoverStore
    disSwiper = new Swiper('.swiper-discover',{
      speed: 300,
      on: {
        slideChangeTransitionStart(){
          changeTabbar(this.activeIndex + '')
        },
      },
    })
  }
  changeSwiper(index) {
    disSwiper.slideTo(index, 300, false)
  }
  render() {
    const { searchHot, tabbarIndex, changeTabbar } = this.props.discoverStore
    return (
      <div className="Discover">
        <Search searchHot={searchHot}></Search>
        <Tabbar tabbarIndex={tabbarIndex} changeTabbar={changeTabbar} changeSwiper={this.changeSwiper}></Tabbar>
        <div className="swiper-container swiper-discover">
          <div className="swiper-wrapper">
            <div className="swiper-slide">
              <Recommend></Recommend>
            </div>
            <div className="swiper-slide">
              <Recommend></Recommend>
            </div>
          </div>
        </div>
        
      </div>
    );
  }
}

export default Discover;