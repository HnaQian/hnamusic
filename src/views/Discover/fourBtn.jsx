import React, {Component} from 'react';
import { observer } from 'mobx-react';
import './../../scss/Discover/fourBtn.scss';

@observer 
class FourBtn extends Component {
  render() {
    return (
      <div className="FourBtn">
        {
          this.props.recommendFourBtn.map((v, k) => {
            return (
              <div className="rebtnCell" key={'four-' + k}>
                <div className={'rebtnCellIcon ' + v.fontIcon}></div>
                <div className="rebtnCellTitle">{v.name}</div>
              </div>
            )
          })
        }
      </div>
    )
  }
}

export default FourBtn;