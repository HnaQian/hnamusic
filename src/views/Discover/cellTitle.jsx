import React, {Component} from 'react';
import { observer } from 'mobx-react';
import './../../scss/Discover/cellTitle.scss';

@observer 
class CellTitle extends Component {
  render() {
    return (
      <div className="CellTitle">
        {this.props.title}
        <i></i>
      </div>
    )
  }
}

export default CellTitle;