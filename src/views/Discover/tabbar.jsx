import React, {Component} from 'react';
import { observer } from 'mobx-react';
import './../../scss/Discover/tabbar.scss';

@observer
class Tabbar extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  onClickTabbar(index) {
    this.props.changeTabbar(index);
    this.props.changeSwiper(index);
  }

  render() {
    const { tabbarIndex } = this.props
    return (
      <div className='tabbar'>
        <div className={'tabbarNameD ' + (tabbarIndex === '0' ? 'tabbarAc' : '')} onClick={() => this.onClickTabbar('0')}>个性推荐</div>
        <div className={'tabbarNameD ' + (tabbarIndex === '1' ? 'tabbarAc' : '')} onClick={() => this.onClickTabbar('1')}>主播电台</div>
        <div className={'tabbarline ' + (tabbarIndex === '0' ? '' : 'tabbarlineA')}></div>
      </div>
    )
  }
}

export default Tabbar;