
import { observable, action } from 'mobx';
import discoverApi from './../api/discover';

class DiscoverStore {
  @observable bannerList;
  @observable searchHot;
  @observable tabbarIndex;
  @observable presonalizedData;//推荐歌单
  constructor() {
    this.bannerList = [];
    this.searchHot = '';
    this.tabbarIndex = '0';
    this.presonalizedData = [];
  }

  @action
  bannerListData = banner => {
    this.bannerList = banner
  }
  @action
  searchHotData = searchKey => {
    this.searchHot = searchKey
  }
  @action
  changeTabbar = index => {
    this.tabbarIndex = index
  }

  @action
  changePresonalizedData = presonalizedData => {
    this.presonalizedData = presonalizedData
  }

  getbannerListData = async () => {
    const banner = await discoverApi.getBannerData();
    this.bannerListData(banner);
  }
  getSearchSuggestData = async () => {
    const searchKey = await discoverApi.searchSuggest();
    this.searchHotData(searchKey[0].first);
  }
  getPresonalizedData = async () => {
    const presonalizedData = await discoverApi.getPresonalizedData();
    this.changePresonalizedData(presonalizedData)
  }
  changeTabbar(index) {
    this.changeTabbar(index)
  }
}

export default new DiscoverStore();